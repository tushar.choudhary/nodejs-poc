const path = require('path')
const express = require('express')
const dotenv = require('dotenv')
const connectDB = require('./config/db.js')
const cors = require('cors')
const swaggerUi = require('swagger-ui-express')
const swaggerDocument = require('./swagger.json')
const socketIo = require('socket.io');
const http = require('http'); 

dotenv.config();

connectDB();

const app = express();

const server = http.createServer(app);
const io = socketIo(server);

io.on('connection', (socket) => {
  console.log('A user connected');

  socket.on('disconnect', () => {
    console.log('A user disconnected');
  });
});

app.use(express.json());
app.use(
  cors({
    origin: "*",
  })
);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/api/users", require('./routes/userRoutes.js'));
app.use("/api/employees", require('./routes/employeesRoutes.js'));

// const __dirname = path.resolve();

app.get("/", (req, res) => {
    res.send("API is Running....");
});


const PORT = process.env.PORT || 5000;
server.listen(
  PORT,
  console.log(`Server running in ${process.env.NODE_ENV} mode on port ${PORT}`)
);

