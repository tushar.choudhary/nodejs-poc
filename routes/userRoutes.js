const express = require('express')
const router = express.Router();
const userControler = require('../controlers/userControler.js');

const authUser = userControler.authUser;
const registerUser = userControler.registerUser;

router.route("/").post(registerUser);

router.post("/login", authUser);


module.exports = router;
