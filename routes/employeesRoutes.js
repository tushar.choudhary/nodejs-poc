const express = require('express')
const router = express.Router();
const userControler = require('../controlers/employeesControler.js');

const getEmployees = userControler.getEmployees;
const deleteEmployee = userControler.deleteEmployee;
const getEmployeeByID = userControler.getEmployeeByID;
const updateEmployee = userControler.updateEmployee;
const registerEmployee = userControler.registerEmployee;

router.route("/").get(getEmployees);

router.route("/register").post(registerEmployee);

router
  .route("/:id")
  .delete(deleteEmployee)
  .get(getEmployeeByID)
  .put(updateEmployee);

  module.exports = router;
