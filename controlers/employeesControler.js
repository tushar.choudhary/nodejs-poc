const asyncHandler = require('express-async-handler')
const Employees = require('../models/employeesModel.js')


const registerEmployee = asyncHandler(async (req, res) => {
    const { name, email, gender, city, dob } = req.body;
  
    const userExists = await Employees.findOne({ email });
  
    if (userExists) {
      res.status(400);
      throw new Error("User already exists");
    }
  
    const user = await User.create({
      name,
      email,
      gender,
      city,
      dob
    });
  
    if (user) {
      res.status(201).json({
        _id: user._id,
        name: user.name,
        email: user.email,
        gender: user.gender,
        city: user.city,
        dob: user.dob,
      });
    } else {
      res.status(400);
      throw new Error("Invalid user data");
    }
  });

const updateEmployee = asyncHandler(async (req, res) => {
  const employee = await Employees.findById(req.params.id);

  if (employee) {
    employee.name = req.body.name || employee.name;
    employee.email = req.body.email || employee.email;
    employee.gender = req.body.gender || employee.gender;
    employee.city = req.body.city || employee.city;
    employee.dob = req.body.dob || employee.dob;

    const updatedEmployee = await user.save();
    res.json({
      _id: updatedEmployee._id,
      name: updatedEmployee.name,
      email: updatedEmployee.email,
      gender: updatedEmployee.gender,
      city: updatedEmployee.city,
      dob: updatedEmployee.dob,
      
    });
  } else {
    res.status(404);
    throw new Error("Employee not found");
  }
});

const getEmployees = asyncHandler(async (req, res) => {
  const employees = await Employees.find({});
  res.json(employees);
});

const getEmployeeByID = asyncHandler(async (req, res) => {
  const employee = await Employees.findById(req.params.id).select("-password");
  if (employee) {
    res.json(employee);
    console.log(employee);
  } else {
    res.status(404);
    throw new Error("Employee not found");
  }
});

const deleteEmployee = asyncHandler(async (req, res) => {
  const employee = await Employees.findById(req.params.id);
  if (employee) {
    await employee.remove();
    res.json({ message: "Employee removed" });
  } else {
    res.status(404);
    throw new Error("Employee not found");
  }
});

module.exports = {
    registerEmployee,
    getEmployees,
    deleteEmployee,
    getEmployeeByID,
    updateEmployee
  };
